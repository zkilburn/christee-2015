//macros_IR.h
#define WallDist 160
#define lowRelateValue 0.8

#define forward true
#define backward false
int lefttemp, righttemp;

inline bool followBoth(int distance, bool direction)
{
  if (continuable)
  {
    int magnitude;
    float sideCorrect;
    float travelDistanceRemaining = frontS - distance;

    PIDTimer.resetTimer();
    PIDTimer2.resetTimer();
    if (direction == forward)
    {
      PID rSideError(WallDist, sideKpR, sideKiR, sideKdR, 0);
      PID lSideError(WallDist, sideKpL, sideKiL, sideKdL, 0);
      while ((frontS >= distance) && (macro_stop != 1))
      {
        //update comms for sensors/ stops
        macroCommunicationsUpdate();
        PIDTimer2.updateTimer();
        if (PIDTimer2.timerDone()) {
          sendMotorCommand(lefttemp,righttemp);
        }
        //update the PID system timer
        PIDTimer.updateTimer();
        if (PIDTimer.timerDone())  //Check if the timer is done
        {
          if (frontS > 200)
            magnitude = motorHighForward;
          else
            magnitude = constrain(constrain((frontS - (distance)), motorLowForward, motorHighForward), motorLowForward, motorHighForward);

          //determine how to correct travel drifting by checking sides
          sideCorrect = rSideError.updateOutput(rightS) - lSideError.updateOutput(leftS);

          //if the side length is not about 150 correct based on length
          if ((!(varIsAbout(WallDist, leftS, sideDeadL))) || (!(varIsAbout(WallDist, rightS, sideDeadR)))) //note that this catch is more for proportional than for derivative
            differentialDrive(formatDifferentialDrive(magnitude, sideCorrect));
          else
            sendMotorCommand(magnitude,magnitude);      
        }
      }
    }
    else
    {
      PID rSideError(WallDist, sideKpR, sideKiR, sideKdR * 0.75, 0);
      PID lSideError(WallDist, sideKpL, sideKiL, sideKdL * 0.75, 0);
      while ((backS >= distance) && (macro_stop != 1))
      {
        //update comms for sensors/ stops
        macroCommunicationsUpdate();
        PIDTimer2.updateTimer();
        if (PIDTimer2.timerDone())
        {
          sendMotorCommand(lefttemp,righttemp);
        }
        //update the PID system timer
        PIDTimer.updateTimer();
        if (PIDTimer.timerDone())  //Check if the timer is done
        {
          if (backS > 200)
            magnitude = motorHighBackward;
          else
            magnitude = constrain(constrain((backS - (distance)), motorHighBackward / 2, motorHighBackward), motorLowBackward, motorHighBackward);

          //determine how to correct travel drifting by checking sides
          sideCorrect = -rSideError.updateOutput(rightS) + lSideError.updateOutput(leftS);

          //if the side length is not about 150 correct based on length
          if ((!(varIsAbout(WallDist, leftS, sideDeadL))) || (!(varIsAbout(WallDist, rightS, sideDeadR)))) //note that this catch is more for proportional than for derivative
            differentialDrive(formatDifferentialDrive(-magnitude, sideCorrect));
          else            
            sendMotorCommand(-magnitude,-magnitude);    
        }
      }
    }
    //end command
    allStop();
    motor_unStick();
    //return if halt
    return (macro_stop == 0);
  }
  else
    return continuable;
}
