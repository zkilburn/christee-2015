  //Navigation.ino

//MAX/MIN FOR MACRO MOTOR
int motorLowForward   = 14;    //with sand 14 // WITHOUT SAND 8
int motorHighForward  = 25;   //with sand 25 // WITHOUT SAND 15
int motorLowBackward  = 11;    //with sand 11 // WITHOUT SAND 5
int motorHighBackward = 20;   //with sand 20 // WITHOUT SAND 11
int motorLowDig       = 18;   //with sand 18 // WITHOUT SAND 8
int motorHighDig      = 30;   //with sand 30 // WITHOUT SAND 15
int motorLowG         = 25;   //with sand 25  // WITHOUT SAND 13
int motorHighG        = 35;   //with sand 35 // WITHOUT SAND 16

#include <FastTransfer.h>
#include <EasyTransfer.h>
#include <EasyTransferCRC.h>
#include <Wire.h>
#include <digitalWriteFast.h>
//#include <Average.h>
#include <PID.h>
#include <Timers.h>
#include "Filter.h"
#include "MPU.h"
//include the .h files
#include "Structs.h"
#include "Methods.h"
#include "Variables.h"
#include "Comms.h"
#include "Motor.h"
#include "Sensors.h"
#include "Macros_Actuator.h"
#include "Macros_Gyro.h"
#include "Macros_Encoders.h"
#include "Macros_Wii.h"
#include "Macros.h"
#include "Debug.h"

void setup() {
  initializeCommunications();
  delay(1000);
  MPU1.MPUinit();
  MPU2.MPUinit();
  initMPUFilters();

}

void loop() {
  updateComms();
  if(MPUTimer.timerDone()) updateMPU();
  //USBdebug();
}



