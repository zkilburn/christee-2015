//macros_actuator.h
int angleSetIn;

inline bool sendActuatorPosition(int angle)
{  
  int count=0;
  while((macro_stop!=1)&&(count<85))
  {
    sendActuatorCommand(angle);
    macroCommunicationsUpdate();
    count++;
    delay(50); 
  }
  sendActuatorCommand(255);

  return (macro_stop == 0);
}

inline bool sendActuatorPositionDig(int angle)
{  
  int count=0;
  while((macro_stop!=1)&&(count<75))
  {
    if(count>15)
    {
    sendMotorCommand(15,15,angle);
    }
    else sendActuatorCommand(angle);
    macroCommunicationsUpdate();
    count++;
    delay(50); 
  }
  sendMotorCommand(0,0,255);

  return (macro_stop == 0);
}

