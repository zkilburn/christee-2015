#define acceptableGyroBeaconAngle 3
#define gyroCorrectBeaconCenterMinAngle 3
#define gyroCorrectBeaconCenterMaxAngle 35
#define ROBOT_ANGLE_CORRECT_WIDE 90


void wiiCameraLocalize(int targetAngle, int cameraNumber)
{
  bool doneLocalizing=false;
  int sweepCount=numberSweeps;
  static Timers decisionTimer(250);
  while(!doneLocalizing&&(macro_stop!=1))
  {
              sweepCount=numberSweeps;
                            
                      //while the wii camera doesnt see it..
                      while((macro_stop!=1)&&(!beaconSeen))
                      {
                        //if we have swept a number of times
                        if((numberSweeps>(sweepCount+2)))
                        {
                          //Reposition for another look
                          doTurn(ROBOT_ANGLE_CORRECT_WIDE);
                          //Wait some sweeps for recheck
                          sweepCount=numberSweeps;
                          delay(200);
                        }
                        //Listen for break and for wii
                        macroCommunicationsUpdate();        
                      }
                        
//                            //Allow to center -- do small adjustments if camera is on edge of capable angle
//                            while(((macro_stop!=1)&&(beaconSeen))&&(!beaconCentered))
//                            {
//                              if(decisionTimer.timerDone())
//                              {
//                                if(beaconAngle<10){
//                                  doTurn(10);
//                                }        
//                                else if((beaconAngle>170)){
//                                  doTurn(-10);
//                                }
//                                decisionTimer.resetTimer();
//                              }
//                              //Listen for break and for wii
//                              macroCommunicationsUpdate(); 
//                            }


                            //Then make angle = what you want;
                            while( (!(((beaconAngle[cameraNumber]-acceptableGyroBeaconAngle)<targetAngle)&&((beaconAngle[cameraNumber]+acceptableGyroBeaconAngle)>targetAngle)))&&((macro_stop!=1)&&(beaconCentered))&&!doneLocalizing)
                            {
                              if(decisionTimer.timerDone())
                              {
                                if(beaconAngle[cameraNumber]<targetAngle)
                                {
                                  doTurn(constrain((targetAngle-beaconAngle[cameraNumber]),gyroCorrectBeaconCenterMinAngle,gyroCorrectBeaconCenterMaxAngle));         
                                }
                                else
                                {
                                  doTurn(constrain((targetAngle-beaconAngle[cameraNumber]),-gyroCorrectBeaconCenterMaxAngle,-gyroCorrectBeaconCenterMinAngle)); 
                                }
                                delay(105);
                                decisionTimer.resetTimer();
                              }
                              //Listen for break and for wii
                              macroCommunicationsUpdate(); 
                              
                              if(( ( (beaconAngle[cameraNumber]-acceptableGyroBeaconAngle) <targetAngle)&&((beaconAngle[cameraNumber]+acceptableGyroBeaconAngle)>targetAngle)))
                              {
                                doneLocalizing=true;
                                break;
                              }
                            }      
    
                            macroCommunicationsUpdate(); 
                          if(( ( (beaconAngle[cameraNumber]-acceptableGyroBeaconAngle) <targetAngle)&&((beaconAngle[cameraNumber]+acceptableGyroBeaconAngle)>targetAngle)))
                            {
                              doneLocalizing=true;
                              break;
                            }
  }

}


