  //From Vishay Tech Support:
  //The following code pertains to TSUS5402 diodes usage from arduino driving pwm to transistor.
  //Up to 1.5A may be possible with pulses <=100us  AND  tp/T<=0.01:
  //But please see also that this is only possible for no high temperatures than T_ambient == 25deg Celsius
  //Datasheet states that if above 60 degreesCelsius, current must be lowered (figure 1 on TSUS5402 datasheet)
  //Mosfet in use is IRF510 (Vishay NPN Mosfet)

#include "TimerOne.h"



//set period to 200us and duty cycle to 50% (max pulse length at 25deg Celcius is 100us at 200us)
//Hook up base of Mosfet to pin 10 on UNO board
//have drain connected to GND
//have source of npn mosfet above 10V (You need to be in the mosfet's saturation region to act as a switch)
                                      //Any lower voltage will be in tride mode where the output will be like an amplifier

//Your output is located between the source pin and the pullup resistor.

/*
                        Vcc
                        |
                        (resistor) value: ([Vcc - (#LEDs * 1.3V)] / 1.5Amps)
                        |
                        |
                        (LEDs)
                        |
                        |
                        |(Mosfet source)
                        |
                        |
arduino pin 10  -----(mosfet base)
                        |                          
                        |
                        |(Mosfet source)
                        |
                        GND
*/

//********IMPORTANT!!!!**********
//have source of mosfet pulled up to Vcc with resistor of value:  (Vcc/ [1.5Amps * #branches in LED circuit])
//hook the LEDs as desired
//In each LED branch, you need resistance of ( [Vcc - (#LEDs * 1.3V)] / 1.5Amps)
//I am guessing Vcc is around 20V???

int i = 0;

void setup()
{
  pinMode(10, OUTPUT);
  pinMode(13, OUTPUT);                // Have the LED pin on UNO board blink when pwm is running (indicates when IR LEDs are on)
  Timer1.initialize(200);             // initialize timer1, and set a 200us period
  Timer1.pwm(9, 490);                 // setup pwm on pin 9, 49% duty cycle to stay a little under 100us pulse length in case operation with higher temperatures
  Timer1.attachInterrupt(callback);   // attaches callback() as a timer overflow interrupt
}

void callback()
{
  digitalWrite(10, digitalRead(10) ^ 1);
  if (i == 1)
  { i = 0;
    digitalWrite(13, LOW);
  }
  else
  { i = 1;
    digitalWrite(13, HIGH);
  }
}

void loop()
{
}
