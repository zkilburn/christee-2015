#include <Timers.h>

//Library constructor
Timers::Timers()
{	
  length=50;
  currentTime=millis();
}
Timers::Timers(int l)
{	
  length=l;
}
//External method
void Timers::resetTimer()
{
	startTime=millis();
	currentTimer=startTime;
}
void Timers::setInterval(int l)
{
	length=l;	
}
void Timers::updateTimer()
{
	currentTime=millis();
}
bool Timers::timerDone()
{
	if (abs(currentTime-startTime)>length)
	{
		resetTimer();
		return true;
	}
	else return false;
}
