#include <PID.h>


PID::PID(float target, float kp,float ki, float kd, int number)
	{
	_number=number;
	_target=target;
    _kp=kp;
	_ki=ki;
	_kd=kd;	
	_past=millis();
    _prevError=0;
	}

//External method
void PID::setPropotionality(float kp, float ki, float kd)
{
	_kp=kp;
	_ki=ki;
	_kd=kd;
}
void PID::clearIntegral()
{
     _integral=0;
}
void PID::verboseCalc()
{
     
    Serial.print(_error);
    Serial.print(",");
    Serial.print(_derivative);
    Serial.print(",");
    Serial.print(_integral);
    Serial.print(",");
    Serial.print(_output);
    Serial.println();
     
     }
void PID::updateTarget(float target)
{
    _target=target;
}
float PID::readDerivative()
{
	return _derivative;	
}
float PID::readError()
{
	return _error;
} 
float PID::readIntegral()
 {
 	return _integral;
 }
 int PID::readNumber()
 {
 	return _number;
 }
float PID::readOutput()
 { 	
 	return _output;
 }
 void PID::clearSystem()
 {
 	_error=0;
 	_prevError=0;
 	_derivative=0;
 	_integral=0;
 	_output=0;
 	_past=millis(); 	
 }
//External method
int PID::updateOutput(float sample)
{
	//update dt in seconds
	_now=millis();
	_dt=(_now-_past)*1000;
	_past=_now;
	
	//update error and dError
	
	_error=_target-sample;	
	_dError=_error-_prevError;
	_derivative= _dError/_dt;
	_integral+= _error*_dt;
	_output=0;
	if(_kp!=0)
    {
	_output+=(_error*_kp);
    }
    if(_kd!=0)
    {    
    _output+=(_derivative*_kd);
    }
    if(_ki!=0)
    {
    _output+=(_integral*_ki);
    } 

    _prevError=_error;
    return _output;
}

