#include <Adafruit_NeoPixel.h>

#define PIN1 5
#define PIN2 6
#define PIN3 11
#define PIN4 10
#define sensor A0

Adafruit_NeoPixel strip1 = Adafruit_NeoPixel(60, PIN1, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip2 = Adafruit_NeoPixel(60, PIN2, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip3 = Adafruit_NeoPixel(60, PIN3, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip4 = Adafruit_NeoPixel(60, PIN4, NEO_GRB + NEO_KHZ800);

void setup() {
  strip1.begin();
  strip2.begin();
  strip3.begin();
  strip4.begin();
  
  strip1.show();
  strip2.show();
  strip3.show();
  strip4.show();
  
  pinMode(sensor, INPUT);
}

void loop() {
  if (analogRead(sensor) > 200) {
  
  colorWipe(strip1.Color(0, 0, 255), 1); 
  //colorWipe(strip1.Color(0, 255, 0), 50);
  //colorWipe(strip1.Color(0, 0, 255), 50);
  
  //theaterChase(strip1.Color(127, 127, 127), 50);
  //theaterChase(strip1.Color(127,   0,   0), 50);
  //theaterChase(strip1.Color(  0,   0, 127), 50);
  
  //rainbow(20);
  //rainbowCycle(20);
  //theaterChaseRainbow(50);
  }
  else {
    colorWipe(strip1.Color(255, 0, 0), 1);  
  }
}

void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip1.numPixels(); i ++) {
     strip1.setPixelColor(i, c);
     strip2.setPixelColor(i, c);
     strip3.setPixelColor(i, c);
     strip4.setPixelColor(i, c);
     strip1.show();
     strip2.show();
     strip3.show();
     strip4.show();
     delay(wait);
  }
}

void rainbow(uint8_t wait) {
  uint16_t i, j;
  
  for(j=0; j<256; j++) {
    for(i=0; i<strip1.numPixels(); i++) {
      strip1.setPixelColor(i, Wheel((i+j) & 255));
      strip2.setPixelColor(i, Wheel((i+j) & 255));
      strip3.setPixelColor(i, Wheel((i+j) & 255));
      strip4.setPixelColor(i, Wheel((i+j) & 255));
    }
    strip1.show();
    strip2.show();
    strip3.show();
    strip4.show();
    delay(wait);
  }
}

void rainbowCycle(uint8_t wait) {
  uint16_t i, j;
  
  for(j=0; j<256*5; j++) {
    for(i=0; i< strip1.numPixels(); i++) {
      strip1.setPixelColor(i, Wheel(((i * 256 / strip1.numPixels()) +j) & 255));
      strip2.setPixelColor(i, Wheel(((i * 256 / strip1.numPixels()) +j) & 255));
      strip3.setPixelColor(i, Wheel(((i * 256 / strip1.numPixels()) +j) & 255));
      strip4.setPixelColor(i, Wheel(((i * 256 / strip1.numPixels()) +j) & 255));
    }
    strip1.show();
    strip2.show();
    strip3.show();
    strip4.show();
    delay(wait);
  }
}

void theaterChase(uint32_t c, uint8_t wait) {
  for (int j=0; j<10; j++) {
    for (int q=0; q < 3; q++) {
      for (int i=0; i < strip1.numPixels(); i=i+3) {
        strip1.setPixelColor(i+q, c);
        strip2.setPixelColor(i+q, c);
        strip3.setPixelColor(i+q, c);
        strip4.setPixelColor(i+q, c);
      }
      strip1.show();
      strip2.show();
      strip3.show();
      strip4.show();
      
      delay(wait);
      
      for (int i=0; i < strip1.numPixels(); i=i+3) {
        strip1.setPixelColor(i+q, 0);
        strip2.setPixelColor(i+q, 0);
        strip3.setPixelColor(i+q, 0);
        strip4.setPixelColor(i+q, 0);
      }
    }
  }
}

void theaterChaseRainbow(uint8_t wait) {
  for (int j=0; j <256; j++) {
    for (int q=0; q < 3; q++) {
      for (int i=0; i < strip1.numPixels(); i=i+3) {
        strip1.setPixelColor(i+q, Wheel( (i+j) % 255));
        strip2.setPixelColor(i+q, Wheel( (i+j) % 255));
        strip3.setPixelColor(i+q, Wheel( (i+j) % 255));
        strip4.setPixelColor(i+q, Wheel( (i+j) % 255));

      }
      strip1.show();
      strip2.show();
      strip3.show();
      strip4.show();
      
      delay(wait);
      
      for (int i=0; i < strip1.numPixels(); i=i+3) {
        strip1.setPixelColor(i=q, 0);
        strip2.setPixelColor(i=q, 0);
        strip3.setPixelColor(i=q, 0);
        strip4.setPixelColor(i=q, 0);
      }
    }
  }
}


uint32_t Wheel(byte WheelPos) {
  if(WheelPos < 85) {
    return strip1.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if(WheelPos < 170) {
    WheelPos -= 85;
    return strip1.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
    WheelPos -+ 170;
    return strip1.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}
        

