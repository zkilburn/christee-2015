/* 
 * File:   ADC.h
 * Author: Igor
 *
 * Created on May 4, 2014, 4:36 PM
 */

#ifndef ADC_H
#define	ADC_H

void InitializeLong(void);
void ShutDown(void);
void InitializeMedium(void);
void StartLong(void);
void StartMedium(void);
#endif	/* ADC_H */

